<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package     BinaryAnvil_DiscountNotification
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\DiscountNotification\Plugin;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\Phrase;
use Product\DiscountNotification\Model\SubscribersRepository;

class LoginPost 
{
    protected $session;
    protected $repo;
    protected $request;

    public function __construct(
        Context $context,
        Session $customerSession,
        SubscribersRepository $repo,
        RequestInterface $request
    ) {
        $this->session = $customerSession;
        $this->repo = $repo;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->resultFactory = $context->getResultFactory();
        $this->request = $request;
    }

    public function afterExecute(\Magento\Customer\Controller\Account\LoginPost $subject, $proceed)
    {         
        if(isset($_SESSION["notf"]) && isset($_SESSION["produrl"]) && isset($_SESSION["sku"])){
           $prevUrl = $_SESSION["produrl"]; 
           $sku = $_SESSION["sku"]; 
           $notifyValue = $_SESSION["notf"];
           $email = $this->session->getCustomer()->getEmail();

            if($notifyValue === "on"){
                $this->repo->setSubscription($sku, $email);
            }else{
                $this->repo->unsetSubscription($sku, $email);  
             }

            unset($_SESSION['sku']);
            unset($_SESSION['notf']);
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath($prevUrl);
            return $resultRedirect;
        }
        else{
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('what-is-new.html');
            return $resultRedirect;
        }
    }
}
