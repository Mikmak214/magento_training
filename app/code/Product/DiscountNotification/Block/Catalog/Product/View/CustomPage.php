<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package     BinaryAnvil_DiscountNotification
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\DiscountNotification\Block\Catalog\Product\View;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session;
use Magento\Customer\Controller\Account\Login;
use Magento\Catalog\Block\Product\View;
use Magento\Framework\Session\SessionManagerInterface;
use Product\DiscountNotification\Model\SubscribersRepository;
use Product\DiscountNotification\Helper\CheckDateValidation;
use Product\DiscountNotification\Logger\Logger;
use Magento\Framework\App\Http\Context;

class CustomPage extends Template
{
    protected $customerSession, $login, $collectionFactory, $getProduct, $session, $repo, $helper, $logger, $httpContext;

    public function __construct(
        Template\Context $context, 
        array $data = [], 
        Session $customerSession, 
        Login $login, 
        View $getProduct,
        SessionManagerInterface $session,
        SubscribersRepository $repo,
        CheckDateValidation $helper,
        Logger $logger,
        Context $httpContext
    ){
        $this->customerSession = $customerSession;
        $this->login = $login;
        $this->getProduct = $getProduct;
        $this->session = $session;
        $this->repo = $repo;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->httpContext = $httpContext;
        parent::__construct($context, $data);
    }

    /**
     * Checking if user is logged in or not
     */
    public function isLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }

    /**
     * Check if user has subscribed to a product
     */
    public function isSubscribed(){
        $sku = $this->getProduct->getProduct()->getSku();
        $email = $this->customerSession->getCustomer()->getEmail();
        return $this->repo->checkSubscription($sku, $email);
    }

    /**
     * Getting product sku and url based on product page
     */
    public function getProduct()
    {
        return $this->getProduct->getProduct();
    }

    public function saleperiodEnd($sku)
    {
        $dateToday = date("z");
        $this->logger->info("DateToday: " . $dateToday);
        $startDate = $this->helper->daysBeforeSale($sku);
        $this->logger->info("StartDate: ");
        $specialPrice = $this->helper->hasSpecialPrice($sku);
        $this->logger->info("Special Price: " . $specialPrice);

        if($dateToday == $startDate && $specialPrice == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function countSku($sku)
    {
        return $this->repo->countSku($sku);
    }
}
