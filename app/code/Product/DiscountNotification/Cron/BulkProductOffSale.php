<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package     BinaryAnvil_DiscountNotification
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\DiscountNotification\Cron;

use Product\DiscountNotification\Model\Config;
use Product\DiscountNotification\Model\SubscribersRepository;
use Product\DiscountNotification\Helper\CheckDateValidation;
use Product\DiscountNotification\Logger\Logger;
use Product\DiscountNotification\Helper\Email;

class BulkProductOffSale
{
    protected $config, $repo, $logger, $emailHelper;

    public function __construct
    (
        Config $_config,
        SubscribersRepository $_repo,
        CheckDateValidation $helper,
        Logger $logger,
        Email $emailhelper
    )
    {
        $this->config = $_config;
        $this->repo = $_repo;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->emailHelper = $emailhelper;
    }

    public function execute()
    {
        if($this->config->isEnabled())
        {
            $subscribersList = $this->repo->getSubscribersSent();

            foreach($subscribersList as $list)
            {
                $sku = $list['productSku'];
                $custEmail = $list['custEmail'];
                $hasSpecialPrice = $this->helper->hasSpecialPrice($sku);

                if($hasSpecialPrice == false)
                {
                    $this->repo->resetProductSubscribers($sku, $custEmail);
                }else{
                    //Do nothing
                }
            }
        }
    }
}
