<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package     BinaryAnvil_DiscountNotification
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\DiscountNotification\Model;

use Product\DiscountNotification\Model\ResourceModel\Subscribers\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResourceConnection;
use Product\DiscountNotification\Logger\Logger;
use Product\DiscountNotification\Helper\CheckDateValidation;

class SubscribersRepository
{
    private $collectionFactory;
    private $dataFactory;
    private $resultRedirect;
    private $resource;
    protected $logger;
    protected $helper;
    private $config;

    public function __construct(
        CollectionFactory $collectionFactory,
        SubscribersFactory $_dataFactory,
        ResultFactory $result,
        ResourceConnection $_resource,
        Logger $logger,
        CheckDateValidation $helper,
        Config $config
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->dataFactory = $_dataFactory;
        $this->resultRedirect = $result;
        $this->resource = $_resource;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->config = $config;
    }

    public function getList()
    {
        return $this->collectionFactory->create()->getCollection();
    }

    /**
     * Setting up subscription of the customer, adding data to custom table
     * @param $sku
     * @param $email
     */
    public function setSubscription($sku, $email)
    {
        $model = $this->collectionFactory->create();
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('customer_product_subscription'); //gives table name with prefix
        $tableName2 = $connection->getTableName('product_subscribers'); //gives table name with prefix

        $checkSku = "SELECT custEmail, productSku FROM $tableName WHERE productSku = '$sku' AND custEmail = '$email'";
        $results1 = $connection->fetchOne($checkSku);

        if(empty($results1))
        {
            $checkCount = "SELECT COUNT(*) AS value FROM $tableName WHERE productSku = '$sku'";
            $dd = $connection->fetchOne($checkCount);
            $num = (int)$dd;

            if($num == 0)
            {
                $num++;
            }

            $listOfSku = "SELECT sku FROM $tableName2 WHERE sku = '$sku'";
            $results2 = $connection->fetchOne($listOfSku);

            if(empty($results2))
            {
                $this->logger->info("Insert: " . $results2);
                $insertSku = "INSERT INTO $tableName2 (sku, subscribers) VALUES ('$sku', '$num')";
                $connection->query($insertSku);
            }
            else
            {
                $nums = $num+1;
                $this->logger->info("Update: " . $results2);
                $updateSku = "UPDATE $tableName2 SET subscribers = '$nums' WHERE sku = '$results2'";
                $connection->query($updateSku);
            }

            $model->getConnection()->insert('customer_product_subscription', ['custEmail' => $email,'productSku' => $sku, 'isAlreadySent' => 0]);
            $model->save();
        }
    }

    /**
     * Removing customer subscription.
     * @param $sku
     * @param $email
     */
    public function unsetSubscription($sku, $email)
    {
        $condition = array(
          'productSku = ?' => $sku, 'custEmail = ?' => $email
        );

        $model = $this->collectionFactory->create();
        $connection = $this->resource->getConnection();
        $tableName2 = $connection->getTableName('product_subscribers'); //gives table name with prefix

        $checkCount = "SELECT subscribers FROM $tableName2 WHERE sku = '$sku'";
        $dd = $connection->fetchOne($checkCount);
        $num = (int)$dd;
        $nums = $num-1;

        //Queries
        $updateCount = "UPDATE $tableName2 SET subscribers = '$nums' WHERE sku = '$sku'";
        $connection->query($updateCount);
        $model->getConnection()->delete('customer_product_subscription', $condition);
        $model->save();

    }

    /**
     * Checking subscription of the user
     * @param $sku
     * @param $email
     * @return bool
     */
    public function checkSubscription($sku, $email)
    {

        $collection = $this->collectionFactory->create()->getItems();

       foreach ($collection as $collect) 
       {
           if($collect['custEmail'] == $email && $collect['productSku'] == $sku)
           {
                return true;
           }
        }         
    }

    /**
     * Getting array of subscribers
     */
    public function getSubscribers()
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('customer_product_subscription'); //gives table name with prefix
        $subscribersList = "SELECT custEmail, productSku, isAlreadySent FROM $tableName";
        $result = $connection->fetchAll($subscribersList);
        return $result;
    }

    /**
     * Getting array of subscribers where isAlreadySent = '1'
     */
    public function getSubscribersSent()
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('customer_product_subscription'); //gives table name with prefix
        $subscribersList = "SELECT custEmail, productSku, isAlreadySent FROM $tableName WHERE isAlreadySent = '1' ";
        $result = $connection->fetchAll($subscribersList);
        return $result;
    }

    /**
     * Getting array of subscribers based on SKU
     * @param $sku
     * @return array
     */
    public function getSubscribersBySku($sku)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('customer_product_subscription'); //gives table name with prefix
        $subscribersList = "SELECT custEmail, productSku FROM $tableName WHERE isAlreadySent = '0' AND productSku = '$sku'";
        $result = $connection->fetchAll($subscribersList);
        return $result;
    }

    /**
     * Removing subscription of a product
     * @param $sku
     * @param $custEmail
     */
    public function resetProductSubscribers($sku, $custEmail)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('customer_product_subscription'); //gives table name with prefix
        $sql = "DELETE FROM $tableName WHERE productSku = '$sku' AND custEmail = '$custEmail'";
        $connection->query($sql);

    }

    /**
     * Updating value of a subscription that has been sent an email
     * @param $sku
     * @param $email
     * @return \Zend_Db_Statement_Interface
     */
    public function alreadySent($sku, $email ,$value)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('customer_product_subscription'); //gives table name with prefix
        $updateList = "UPDATE $tableName SET isAlreadySent = '$value' WHERE productSku = '$sku' AND custEmail = '$email'";
        $result = $connection->query($updateList);
        return $result;
    }

    /**
     * Sample function for counting the number of SKU in table
     */
    public function countSku($sku)
    {
        $collection = $this->dataFactory->create()
            ->getCollection();

        return "HAHAHAHAHA";
    }
}