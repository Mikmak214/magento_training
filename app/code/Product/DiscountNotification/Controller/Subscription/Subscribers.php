<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package     BinaryAnvil_DiscountNotification
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\DiscountNotification\Controller\Subscription;

use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Controller\Account\Login;
use Magento\Framework\UrlInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Product\DiscountNotification\Model\SubscribersRepository;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\View\Result\PageFactory;

class Subscribers extends \Magento\Framework\App\Action\Action
{
    protected $customerSession, $url, $session, $repo, $resultRedirect, $login, $resultPageFactory;

    public function __construct(
        Session $customerSession,
        UrlInterface $urlInterface,
        SessionManagerInterface $session,
        SubscribersRepository $repo,
        Context $context,
        Redirect $_resultRedirect,
        Login $_login,
        PageFactory $_resultPageFactory
    ){
        $this->customerSession = $customerSession;
        $this->url = $urlInterface;
        $this->session = $session;
        $this->repo = $repo;
        $this->resultRedirect = $_resultRedirect;
        $this->login = $_login;
        $this->resultPageFactory = $_resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $sku = $this->getRequest()->getParam("productsku");
        $notifyValue = $this->getRequest()->getParam("notifyme");
        $prevUrl = $this->getRequest()->getParam("producturl");
        $email = $this->customerSession->getCustomer()->getEmail();

        if($notifyValue === "on"){
            $this->repo->setSubscription($sku, $email);
        }else{
            $this->repo->unsetSubscription($sku, $email);  
        }
        $this->_redirect($prevUrl);
    }
}