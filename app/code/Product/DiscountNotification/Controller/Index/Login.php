<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package     BinaryAnvil_DiscountNotification
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\DiscountNotification\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\UrlInterface;

class Login extends \Magento\Framework\App\Action\Action
{
    protected $session;

    public function __construct(
        Context $context,
        Session $customerSession
    ) {
        $this->session = $customerSession;
        parent::__construct($context);
         
    }

    /**
     * Redirect to LoginPost
     */
    public function execute()
    {
        $sku = $this->getRequest()->getParam("productsku");
        $notifyValue = $this->getRequest()->getParam("notifyme");
        $prevUrl = $this->getRequest()->getParam("producturl");
        $_SESSION["sku"] = $sku;
        $_SESSION["notf"] = $notifyValue;
        $_SESSION["produrl"] = $prevUrl;
        $this->_redirect('customer/account/login');
    }
}