<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package     BinaryAnvil_DiscountNotification
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\DiscountNotification\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Product\DiscountNotification\Model\SubscribersRepository;
use Product\DiscountNotification\Logger\Logger;

class Email extends AbstractHelper
{
	protected $inlineTranslation, $escaper, $transportBuilder, $logger, $repo, $_logger;

    /**
     * @param Context $context
     * @param StateInterface $inlineTranslation
     * @param Escaper $escaper
     * @param TransportBuilder $transportBuilder
     * @param SubscribersRepository $repo
     * @param Logger $_logger
     */
    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        SubscribersRepository $repo,
        Logger $_logger

    ){
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->repo = $repo;
        $this->_logger = $_logger;
        $this->logger = $context->getLogger();
    }

    /**
     * Sending email notification to subscribers of a product
     * @param $emailTemplate
     * @param $sku
     * @param $custEmail
     */
    public function sendEmail($emailTemplate, $sku, $custEmail)
    {
        $this->_logger->info("A: " . $custEmail);
        try {
            $this->inlineTranslation->suspend();
            $sender = [
                'name' => $this->escaper->escapeHtml('Luma'),
                'email' => $this->escaper->escapeHtml('LumaSales@gmail.com'),
            ];

            $templateVars = [
                'templateSku'  => $sku,
                'templateEmail' => $custEmail
            ];
            $this->_logger->info("B: " . $emailTemplate);
            $transport = $this->transportBuilder
                ->setTemplateIdentifier($emailTemplate)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars($templateVars)
                ->setFrom($sender)
                ->addTo($custEmail)
                ->getTransport();
            $this->_logger->info("C: " . $sku);
            $transport->sendMessage();
            $this->_logger->info("D");
            $this->inlineTranslation->resume();
            $this->repo->alreadySent($sku, $custEmail, '1');
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }
}

