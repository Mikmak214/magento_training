<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package     BinaryAnvil_DiscountNotification
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\DiscountNotification\Observer;

use Product\DiscountNotification\Model\Config;
use Magento\Framework\Event\ObserverInterface;
use Product\DiscountNotification\Logger\Logger;
use Product\DiscountNotification\Model\SubscribersRepository;
use Magento\Customer\Model\Session;
use Product\DiscountNotification\Helper\CheckDateValidation;
use Product\DiscountNotification\Helper\Email;
use Product\DiscountNotification\Cron\BulkSendNotification;
use Magento\Catalog\Api\ProductRepositoryInterface;

class ProductSaveAfter implements ObserverInterface
{    
	protected $config, $logger, $repo, $customersession, $helper, $emailHelper, $bulksend, $repository;

	public function __construct(
		Config $_config,
        Logger $logger,
        SubscribersRepository $repo,
        Session $customersession,
        CheckDateValidation $helper,
        Email $emailHelper,
        BulkSendNotification $bulksend,
        ProductRepositoryInterface $repository
	)
  	{
    	$this->config = $_config;
        $this->logger = $logger;
        $this->repo = $repo;
        $this->helper = $helper;
        $this->emailHelper = $emailHelper;
        $this->bulksend = $bulksend;
        $this->repository = $repository;
  	}

    /**
     * Execute for sending emails
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sku = $observer->getProduct()->getSku();  // you will get product object
        $url = $this->productRepository->get($sku);
        $this->logger->info("Product URL: " . $url);


        if($this->config->isEnabled())
        {
            $subscribersList = $this->repo->getSubscribersBySku($sku);

            foreach($subscribersList as $list)
            {
                $hasSpecialPrice = $this->helper->hasSpecialPrice($sku);

                if ($hasSpecialPrice == true) {
                    $dateToday = date("z");
                    $startDate = $this->helper->daysBeforeSale($sku);
                    $daysBeforeSale = ($startDate - $dateToday);
                    $leadTime = $this->config->getLeadTime();
                    $custEmail = $list['custEmail'];
                    $this->logger->info($custEmail);

                    if ($leadTime >= $daysBeforeSale && $daysBeforeSale > 0)
                    {
                        $this->logger->info("LEADTIMEEEEEEE!!!!!!!");
                        $emailTemplate = 'email_demo_template';
                        $this->emailHelper->sendEmail($emailTemplate, $sku, $custEmail);
                    }
                    else if ($startDate == $dateToday)
                    {
                        $this->logger->info("Start Date!!!!!!!");
                        $emailTemplate = 'email_demo_templatee';
                        $this->emailHelper->sendEmail($emailTemplate, $sku, $custEmail);
                    }
                }
            }
        }
    }
}