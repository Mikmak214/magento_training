<?php

namespace Product\AffiliateSales\Model\ResourceModel\Withdrawals;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Product\AffiliateSales\Model\Withdrawals;
use Product\AffiliateSales\Model\ResourceModel\Withdrawals as WithdrawalsResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(Withdrawals::class, WithdrawalsResource::class);
    }
}
