<?php

namespace Product\AffiliateSales\Model\ResourceModel\Commission;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Product\AffiliateSales\Model\Commission;
use Product\AffiliateSales\Model\ResourceModel\Commission as CommissionResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(Commission::class, CommissionResource::class);
    }
}
