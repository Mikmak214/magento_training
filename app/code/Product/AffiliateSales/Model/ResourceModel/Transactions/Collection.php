<?php

namespace Product\AffiliateSales\Model\ResourceModel\Transactions;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Product\AffiliateSales\Model\Transactions;
use Product\AffiliateSales\Model\ResourceModel\Transactions as TransactionsResource;

class Collection extends AbstractCollection
{
	protected $_idFieldName = 'id';

	protected function _construct()
	{
		$this->_init(Transactions::class, TransactionsResource::class);
	}
}
