<?php

namespace Product\AffiliateSales\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Commission extends AbstractDb
{
    public function _construct()
    {
        $this->_init('affiliate_commission', 'id');
    }
}
