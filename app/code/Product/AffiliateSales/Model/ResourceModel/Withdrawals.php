<?php

namespace Product\AffiliateSales\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Withdrawals extends AbstractDb
{
    public function _construct()
    {
        $this->_init('affiliate_withdrawals', 'id');
    }
}
