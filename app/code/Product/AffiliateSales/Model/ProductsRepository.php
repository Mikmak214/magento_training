<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package      BinaryAnvil_AffiliateSales
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\AffiliateSales\Model;

use Product\AffiliateSales\Api\ProductsRepositoryInterface;
use Product\AffiliateSales\Model\ResourceModel\Products\CollectionFactory;
use Magento\Framework\App\ResourceConnection;

class ProductsRepository implements ProductsRepositoryInterface
{

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * ProductsRepository constructor.
     * @param CollectionFactory $collectionFactory
     * @param ResourceConnection $resource
     */
    public function __construct(CollectionFactory $collectionFactory,
                                ResourceConnection $resource)
    {
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
    }

    /**
     * @return \Magento\Framework\DataObject[]|\Product\AffiliateSales\Api\Data\ProductsInterface[]
     */
    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    /**
     * @param $title
     * @param $affiliate_product_link
     * @param $product_sku
     * @param $commission_rate
     */
    public function setAffiliates($title, $affiliate_product_link, $product_sku, $commission_rate)
    {
        $model = $this->collectionFactory->create();

        $model->getConnection()->insert('affiliate_products', ['title' => $title, 'affiliate_product_link' => $affiliate_product_link
            , 'product_sku' => $product_sku, 'commission_rate' => $commission_rate]);
        $model->save();
    }

    /**
     * @param $trackingCode
     * @param $sku
     * @param $commission_rate
     */
    public function setAffiliateSku($trackingCode, $sku, $commission_rate)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_sku'); //gives table name with prefix

        $check = "SELECT trackingcode, productsku FROM $tableName WHERE trackingcode = '$trackingCode' AND productsku = '$sku'";
        $results1 = $connection->fetchOne($check);

        if (empty($results1)) {
            $insert = "INSERT INTO $tableName (trackingcode, productsku, commission_rate) VALUES ('$trackingCode', '$sku', '$commission_rate')";
            $results = $connection->query($insert);
        }
    }

    /**
     * @param $sku
     * @return bool
     */
    public function checkProductSku($sku)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_sku'); //gives table name with prefix

        $check = "SELECT productsku FROM $tableName WHERE productsku = '$sku'";
        $results1 = $connection->fetchAll($check);

        if (empty($results1)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $sku
     * @param $trackingcode
     * @return string
     */
    public function getCampaignType($sku, $trackingcode)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_sku'); //gives table name with prefix

        $check = "SELECT commission_rate FROM $tableName WHERE productsku = '$sku' AND trackingcode = '$trackingcode'";
        return $campaigntype = $connection->fetchOne($check);
    }

    /**
     * @param $sku
     * @return array
     */
    public function getTrackingCode($sku)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliated_sku'); //gives table name with prefix

        $check = "SELECT trackingcode, commission_rate FROM $tableName WHERE productsku = '$sku'";
        return $list = $connection->fetchAll($check);
    }
}

