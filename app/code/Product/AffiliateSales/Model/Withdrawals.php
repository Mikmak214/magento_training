<?php

namespace Product\AffiliateSales\Model;

use Magento\Framework\Model\AbstractModel;

class Withdrawals extends AbstractModel
{
    protected $_eventPrefix = 'affiliate_withdrawals';

    public function _construct()
    {
        $this->_init(\Product\AffiliateSales\Model\ResourceModel\Withdrawals::class);
    }
}
