<?php

namespace Product\AffiliateSales\Model;

use Magento\Framework\Model\AbstractModel;

/**
 *
 */
class Commission extends AbstractModel
{
    protected $_eventPrefix = 'affiliate_commission';

    public function _construct()
    {
        $this->_init(\Product\AffiliateSales\Model\ResourceModel\Commission::class);
    }
}
