<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package      BinaryAnvil_AffiliateSales
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\AffiliateSales\Model;

use Magento\Framework\DataObject;
use Product\AffiliateSales\Api\AccountsRepositoryInterface;
use Product\AffiliateSales\Api\Data\AccountsInterface;
use Product\AffiliateSales\Model\ResourceModel\Accounts\CollectionFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory as RuleCollection;
use Product\AffiliateSales\Logger\Logger;

class AccountsRepository implements AccountsRepositoryInterface
{
    /**
     * @var ResourceConnection
     */
    protected $resource;
    /**
     * @var RuleCollection
     */
    protected $ruleFactory;

    protected $lastInsertId;
    /**
     * @var CollectionFactory
     */

    /**
     * @var Logger
     */
    protected $logger;

    private $collectionFactory;

    /**
     * AccountsRepository constructor.
     * @param CollectionFactory $collectionFactory
     * @param ResourceConnection $resource
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        ResourceConnection $resource,
        RuleCollection $ruleFactory,
        Logger $logger
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
        $this->ruleFactory = $ruleFactory;
        $this->logger = $logger;
    }

    /**
     * @return DataObject[]|AccountsInterface[]
     */
    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    /**
     * Save affiliates account information to db upon registration with trackingcode
     *
     * @param $firstname
     * @param $lastname
     * @param $email_address
     * @param $trackingcode
     */
    public function setAffiliates($firstname, $lastname, $email_address, $trackingcode)
    {
        $model = $this->collectionFactory->create();

        $model->getConnection()->insert('affiliate_accounts', ['firstname' => $firstname, 'lastname' => $lastname,
            'email_address' => $email_address, 'trackingcode' => $trackingcode, 'commision_rate' => '0']);

        $model->save();
    }

    /**
     * @param $discount_amount
     */
    public function setDiscountAmount($name, $description, $discount_amount, $is_active, $simple_action, $coupon_type)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('salesrule'); //gives table name with prefix
        $insert = "INSERT INTO $tableName(name, description, discount_amount, is_active, simple_action, coupon_type)
                  VALUES ('$name', '$description', '$discount_amount', '$is_active', '$simple_action', '$coupon_type')";
        $list = $connection->query($insert);
    }


    public function setCustomerGroup()
    {
        $connection = $this->resource->getConnection();
        $salesrule_customer = $connection->getTableName('salesrule_customer_group');
        $salesrule = $connection->getTableName('salesrule');
        $customer_group = $connection->getTableName('customer_group');

        $query = "INSERT INTO $salesrule_customer (rule_id, customer_group_id) VALUES 
                    ((SELECT rule_id FROM $salesrule ORDER BY rule_id DESC LIMIT 1), 
                    (SELECT customer_group_id FROM $customer_group WHERE customer_group_id = 1))";
        $connection->query($query);
    }

    public function setWebsite()
    {
        $connection = $this->resource->getConnection();
        $salesrule_website = $connection->getTableName('salesrule_website');
        $salesrule = $connection->getTableName('salesrule');
        $store_website = $connection->getTableName('store_website');

        $query = "INSERT INTO $salesrule_website (rule_id, website_id) VALUES 
                ((SELECT rule_id FROM $salesrule ORDER BY rule_id DESC LIMIT 1),
                (SELECT website_id FROM $store_website WHERE website_id = 1))";
        $connection->query($query);
    }

    /**
     * @param $code
     * @param $is_primary
     * @internal param $rule_id
     */
    public function setCouponCode($code, $is_primary)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('salesrule_coupon');
        $salesrule = $connection->getTableName('salesrule');

        $query = "INSERT INTO $tableName (rule_id, code, is_primary) VALUES 
                    ((SELECT rule_id FROM $salesrule ORDER BY rule_id DESC LIMIT 1), '$code', '$is_primary')";
        $connection->query($query);
    }

    /**
     * Get the list of affiliate codes
     *
     * @return array
     */
    public function getTrackingCode()
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_accounts'); //gives table name with prefix

        $check = "SELECT trackingcode FROM $tableName";
        return $list = $connection->fetchAll($check);
    }

    /**
     * Save affiliates commission and reward points to db
     *
     * @param $trackingcode
     * @param $commisionrate
     */
    public function setCommisionRate($trackingcode, $commisionrate, $reward_points)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_accounts'); //gives table name with prefix

        $setRate = "UPDATE $tableName SET commision_rate =  '$commisionrate' WHERE trackingcode = '$trackingcode'";
        $setRewards = "UPDATE $tableName SET reward_points =  '$reward_points' WHERE trackingcode = '$trackingcode'";

        $connection->query($setRate);
        $connection->query($setRewards);
    }

    /**
     * @param $trackingcode
     * @return string
     */
    public function getCommissionRate($trackingcode)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_accounts'); //gives table name with prefix

        $check = "SELECT commision_rate FROM $tableName WHERE trackingcode = '$trackingcode'";
        return $list = $connection->fetchOne($check);
    }


}
