<?php

namespace Product\AffiliateSales\Model;

use Magento\Framework\Model\AbstractModel;

/**
 *
 */
class Accounts extends AbstractModel
{
	protected $_eventPrefix = 'affiliate_accounts';

	public function _construct()
	{
		$this->_init(\Product\AffiliateSales\Model\ResourceModel\Accounts::class);
	}
}
