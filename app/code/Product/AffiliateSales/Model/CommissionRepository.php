<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package      BinaryAnvil_AffiliateSales
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\AffiliateSales\Model;

use Magento\Framework\App\ResourceConnection;
use Product\AffiliateSales\Api\CommissionRepositoryInterface;
use Product\AffiliateSales\Model\ResourceModel\Commission\CollectionFactory;

class CommissionRepository
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * CommissionRepository constructor.
     * @param CollectionFactory $collectionFactory
     * @param ResourceConnection $resource
     */
    public function __construct(CollectionFactory $collectionFactory, ResourceConnection $resource)
    {
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
    }

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    /**
     * @param $commission_title
     * @param $commission_rate
     */
    public function setAffiliates($commission_title, $commission_rate)
    {
        $model = $this->collectionFactory->create();

        $model->getConnection()->insert('affiliate_commission', [
            'commission_title' => $commission_title,
            'commission_rate' => $commission_rate]
        );
        $model->save();
    }

    /**
     * @param $title
     * @return string
     */
    public function getCommisionRate($title)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_commission'); //gives table name with prefix

        $commisionrate = "SELECT commission_rate FROM $tableName WHERE commission_title = '$title'";
        return $results1 = $connection->fetchOne($commisionrate);
    }

    /**
     * @return array
     */
    public function getCommisionTitle()
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_commission'); //gives table name with prefix

        $commisionrate = "SELECT commission_title FROM $tableName";
        return $results1 = $connection->fetchAll($commisionrate);
    }

    /**
     * @param $trackingcode
     * @param $sku
     * @param $description
     * @param $date
     * @param $price
     * @param $quantity
     * @param $total
     */
    public function setTransaction($trackingcode, $sku, $description, $date, $price, $quantity, $total)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_transaction'); //gives table name with prefix

        $commisionrate = "INSERT INTO $tableName(trackingcode, sku, description, date, price, quantity, comissiontotal) 
                          VALUES ('$trackingcode', '$sku', '$description', '$date', '$price', '$quantity', '$total')";
        $results1 = $connection->query($commisionrate);
    }

    /**
     * @param $commisionRate
     * @param $price
     * @param $quantity
     * @return float|int
     */
    public function getPercentage($commisionRate, $price, $quantity)
    {
        $percentage = $commisionRate / 100;
        $total = $price * $quantity;

        return $percent = $percentage * $total;
    }

    /**
     * @param $trackingcode
     * @return int|string
     */
    public function getAllCommission($trackingcode)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_transaction'); //gives table name with prefix

        $commisionrate = "SELECT comissiontotal FROM $tableName WHERE trackingcode = '$trackingcode'";
        $results = $connection->fetchAll($commisionrate);
        $sum = 0;

        foreach ($results as $result) {
            $sum = $sum+implode($result);
        }
        return $sum;
    }/**
     *  Checking the email of an affiliate
     */
    public function checkEmail($email)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_accounts'); //gives table name with prefix

        $check = "SELECT email_address FROM $tableName WHERE email_address = '$email'";
        $list = $connection->fetchOne($check);

        if(empty($list))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
