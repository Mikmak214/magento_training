<?php

namespace Product\AffiliateSales\Model;

use Magento\Framework\Model\AbstractModel;

class Transactions extends AbstractModel
{
    protected $_eventPrefix = 'affiliate_transaction';

    public function _construct()
    {
        $this->_init(\Product\AffiliateSales\Model\ResourceModel\Transactions::class);
    }
}
