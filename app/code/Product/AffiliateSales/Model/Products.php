<?php

namespace Product\AffiliateSales\Model;

use Magento\Framework\Model\AbstractModel;

class Products extends AbstractModel
{
    protected $_eventPrefix = 'affiliate_products';

    public function _construct()
    {
        $this->_init(\Product\AffiliateSales\Model\ResourceModel\Products::class);
    }
}
