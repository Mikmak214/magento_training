<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package      BinaryAnvil_AffiliateSales
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\AffiliateSales\Plugin;

use Magento\Customer\Controller\Account\CreatePost;
use Product\AffiliateSales\Model\AccountsRepository;
use Product\AffiliateSales\Block\Hello;
use Magento\Framework\App\Action\AbstractAction;
use Product\AffiliateSales\Logger\Logger;
use Magento\Framework\App\Action\Context;



class Trackingcode
{
    /**
     * @var AccountsRepository
     */
    protected $repo;

    /**
     * @var Hello
     */
    protected $view;

    /**
     * @var AbstractAction
     */
    protected $request;

    /**
     * @var Logger
     */
    protected $logger;

    protected $salesrule;

    /**
     * Trackingcode constructor.
     * @param Context $context
     * @param AccountsRepository $repo
     * @param Hello $view
     * @param Logger $logger
     * @param AbstractAction $request
     */
    public function __construct(
        Context $context,
        AccountsRepository $repo,
        Hello $view,
        Logger $logger,
        AbstractAction $request
    ) {
        $this->repo = $repo;
        $this->view=$view;
        $this->request= $request;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->logger = $logger;
    }

    /**
     * @param int $length
     * @return string
     */
    function generateRandomCode($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = 'BAAF-';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param CreatePost $createPost
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function beforeExecute(CreatePost $createPost)
    {
        $fname = $this->request->getRequest()->getParam('firstname');
        $lname = $this->request->getRequest()->getParam('lastname');
        $email = $this->request->getRequest()->getParam('email');
        $trackingcode = $this->generateRandomCode();

        $name = "Affiliate Discount Code: ".$trackingcode;
        $description = "Affiliate Discount Code - 25% Discount for Orders";
        $discount_amount = 25;
        $is_active = 1;
        $simple_action = 'by_percent';
        $coupon_type = 2;
        $choice = $this->request->getRequest()->getParam('regulation');
        $this->logger->info($choice);

        if($choice === 'on')
        {
            $this->repo->setAffiliates($fname, $lname, $email, $trackingcode);
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('what-is-new.html');
        return $resultRedirect;

    }
}
