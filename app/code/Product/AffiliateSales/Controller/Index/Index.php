<?php

namespace Product\AffiliateSales\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;



class Index extends \Magento\Backend\App\Action
{
	public function execute()
	{
        $this->_view->loadLayout();
        $this->_view->renderLayout();
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
	}
}
