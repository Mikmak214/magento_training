<?php
namespace Product\AffiliateSales\Controller\Adminhtml\Accounts;

use Product\AffiliateSales\Model\AccountsFactory;
use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var AccountsFactory
     */
    private $accountsFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param AccountsFactory $accountsFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        AccountsFactory $accountsFactory
    ) {
        $this->accountsFactory = $accountsFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|
     * \Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $this->accountsFactory->create()
            ->setData($this->getRequest()->getPostValue()['general'])
            ->save();
        return $this->resultRedirectFactory->create()->setPath('affiliates/index/index');
    }
}
