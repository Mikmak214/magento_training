<?php
namespace Product\AffiliateSales\Controller\Adminhtml\Commission;

use Product\AffiliateSales\Model\CommissionFactory;
use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var CommissionFactory
     */
    private $commissionFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param CommissionFactory $commissionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        CommissionFactory $commissionFactory
    ) {
        $this->commissionFactory = $commissionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\
     * Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->commissionFactory->create()
            ->setData($this->getRequest()->getPostValue()['general'])
            ->save();
        return $this->resultRedirectFactory->create()->setPath('commission/index/index');
    }
}
