<?php
namespace Product\AffiliateSales\Controller\Adminhtml\Products;

use Product\AffiliateSales\Model\ProductsFactory;
use Magento\Backend\App\Action;

class Save extends Action
{

    /**
     * @var ProductsFactory
     */
    private $productsFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param ProductsFactory $productsFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        ProductsFactory $productsFactory
    ) {
        $this->productsFactory = $productsFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\
     * Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $this->productsFactory->create()
            ->setData($this->getRequest()->getPostValue()['general'])
            ->save();
        return $this->resultRedirectFactory->create()->setPath('products/index/index');
    }
}
