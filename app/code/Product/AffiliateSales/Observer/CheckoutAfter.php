<?php

/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package      BinaryAnvil_AffiliateSales
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\AffiliateSales\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Product\AffiliateSales\Logger\Logger;
use Product\AffiliateSales\Model\ProductsRepository;
use Product\AffiliateSales\Model\AccountsRepository;
use Product\AffiliateSales\Model\CommissionRepository;
use Product\AffiliateSales\Block\Display;

class CheckoutAfter implements ObserverInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var ProductsRepository
     */
    protected $productsrepo;

    /**
     * @var AccountsRepository
     */
    protected $accountsrepo;

    /**
     * @var CommissionRepository
     */
    protected $commissionrepo;

    /**
     * @var Display
     */
    protected $rewards;

    /**
     * CheckoutAfter constructor.
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param Logger $logger
     * @param ProductsRepository $productsrepo
     * @param AccountsRepository $accountsrepo
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepositoryInterface,
        Logger $logger,
        ProductsRepository $productsrepo,
        AccountsRepository $accountsrepo,
        CommissionRepository $commissionrepo,
        Display $rewards
    ) {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->logger = $logger;
        $this->productsrepo = $productsrepo;
        $this->accountsrepo = $accountsrepo;
        $this->commissionrepo = $commissionrepo;
        $this->rewards = $rewards;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $items = $order->getAllVisibleItems();
        foreach ($items as $item) {
            $sku = $item->getSku();
            $price = $item->getPrice();
            $quantity = $item->getQtyOrdered();
            $order = $observer->getEvent()->getOrder();
            $orderId = $order->getId();

            $exist = $this->productsrepo->checkProductSku($sku);

            if ($exist == true) {
                $listofdata = $this->productsrepo->getTrackingCode($sku);

                foreach ($listofdata AS $list) {
                    $listofaffiliates = $this->accountsrepo->getTrackingCode();

                    foreach ($listofaffiliates AS $listt) {
                        if ($list['trackingcode'] == $listt['trackingcode']) {
                            $campaigntype = $this->productsrepo->getCampaignType($sku, $list['trackingcode']);
                            $this->logger->info($campaigntype);
                            $commisionRate = $this->commissionrepo->getCommisionRate($campaigntype);
                            $this->logger->info($commisionRate);
                            $skuCommision = $this->commissionrepo->getPercentage($commisionRate, $price, $quantity);
                            $this->logger->info($skuCommision);
                            $description = "Commission Order Number" . $orderId;

                            $reward_points = $this->rewards->getRewards($list['trackingcode']);
                            $reward_points++;

                            $this->commissionrepo->setTransaction(
                                $list['trackingcode'],
                                $sku,
                                $description,
                                date("Y/m/d"),
                                $price,
                                $quantity,
                                $skuCommision
                            );

                            $commisionRatee = $this->commissionrepo->getAllCommission($list['trackingcode']);
                            $this->accountsrepo->setCommisionRate($list['trackingcode'], $commisionRatee, $reward_points);
                        }
                    }
                }
            }
        }
    }
}
