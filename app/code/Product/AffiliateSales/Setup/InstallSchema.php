<?php

namespace Product\AffiliateSales\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
	/**
	* {@inheritdoc}
	*/
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();

		$table1 = $setup->getConnection()->newTable(
			$setup->getTable('affiliate_accounts')
		)->addColumn(
			'id',
			Table::TYPE_INTEGER,
			null,
			['identity' => true, 'nullable' => false, 'primary' => true],
			'Item ID'
		)->addColumn(
			'firstname',
			Table::TYPE_TEXT,
			255,
			['nullable' >= false],
			'First Name'
		)->addColumn(
			'lastname',
			Table::TYPE_TEXT,
			255,
			['nullable' >= false],
			'Last Name'
		)->addColumn(
			'email_address',
			Table::TYPE_TEXT,
			255,
			['nullable' >= false],
			'Email Address'
		)->addColumn(
			'trackingcode',
			Table::TYPE_TEXT,
			255,
			['nullable' >= false],
			'Tracking Code'
		)->addColumn(
            'commision_rate',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Commission Rate'
        )->setComment(
			'Manage Affiliate Account'
		);

        $table2 = $setup->getConnection()->newTable(
            $setup->getTable('affiliate_products')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Product ID'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Title'
        )->addColumn(
            'affiliate_product_link',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Product Link'
        )->addColumn(
            'product_sku',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Product Sku'
        )->setComment(
            'Manage Affiliate Product'
        );


        $table3 = $setup->getConnection()->newTable(
            $setup->getTable('affiliated_sku')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Product ID'
        )->addColumn(
            'trackingcode',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Tracking Code'
        )->addColumn(
            'productsku',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Product Sku'
        )->addColumn(
            'commission_rate',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Commission Rate'
        )->setComment(
            'Manage Affiliate Product'
        );

        $table4 = $setup->getConnection()->newTable(
            $setup->getTable('affiliate_commission')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Product ID'
        )->addColumn(
            'commission_title',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Commission Title'
        )->addColumn(
            'commission_rate',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Commission Rate'
        )->setComment(
            'Manage Affiliate Commission'
        );

        $table5 = $setup->getConnection()->newTable(
            $setup->getTable('affiliate_transaction')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Product ID'
        )->addColumn(
            'trackingcode',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Tracking Code'
        )->addColumn(
            'sku',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Sku'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Description'
        )->addColumn(
            'date',
            Table::TYPE_DATE,
            255,
            ['nullable' >= false],
            'Date'
        )->addColumn(
            'price',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Price'
        )->addColumn(
            'quantity',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Quantity'
        )->addColumn(
            'comissiontotal',
            Table::TYPE_TEXT,
            255,
            ['nullable' >= false],
            'Commission Total'
        )->setComment(
            'Manage Affiliate Commission'
        );

		$setup->getConnection()->createTable($table1);
        $setup->getConnection()->createTable($table2);
        $setup->getConnection()->createTable($table3);
        $setup->getConnection()->createTable($table4);
        $setup->getConnection()->createTable($table5);
		$setup->endSetup();
	}
}
