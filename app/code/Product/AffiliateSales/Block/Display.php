<?php
/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category    BinaryAnvil
 * @package      BinaryAnvil_AffiliateSales
 * @copyright   Copyright (c) 2015-current Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license     http://www.binaryanvil.com/software/license
 */

namespace Product\AffiliateSales\Api\Data;

namespace Product\AffiliateSales\Block;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Product\AffiliateSales\Model\AccountsFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Request\Http;
use Product\AffiliateSales\Model\Products;
use Product\AffiliateSales\Model\ProductsRepository;
use Product\AffiliateSales\Model\CommissionRepository;
use Product\AffiliateSales\Model\TransactionsFactory;
use Product\AffiliateSales\Model\WithdrawalsFactory;
use Temando\Shipping\Model\ResourceModel\Batch\OrderCollectionFactory;


class Display extends Template
{

    /**
     * @var Accounts
     */
    protected $_accounts;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var OrderCollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Repository
     */
    protected $repo;

    /**
     * @var CommissionRepository
     */
    protected $commissionrepo;

    /**
     * @var TransactionsRepository
     */
    protected $transactionorders;

    /**
     * @var WithdrawalsFactory
     */
    protected $withdrawals;

    /**
     * @var Products
     */
    private $_products;

    private $resource;

    /**
     * Display constructor.
     * @param Context $context
     * @param CollectionFactory $orderCollectionFactory
     * @param Products $product
     * @param ResourceConnection $resource
     * @param AccountsFactory $accounts
     * @param ProductsRepository $repo
     * @param Session $customerSession
     * @param Http $request
     * @param CommissionRepository $commissionrepo
     * @param TransactionsFactory $transactionorders
     * @param WithdrawalsFactory $withdrawals
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $orderCollectionFactory,
        Products $product,
        ResourceConnection $resource,
        AccountsFactory $accounts,
        ProductsRepository $repo,
        Session $customerSession,
        Http $request,
        CommissionRepository $commissionrepo,
        TransactionsFactory $transactionorders,
        WithdrawalsFactory $withdrawals,
        array $data = []
    )
    {
        $this->_accounts = $accounts;
        $this->_products = $product;
        $this->_resource = $resource;
        $this->repo = $repo;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->request = $request;
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->commissionrepo = $commissionrepo;
        $this->transactionorders = $transactionorders;
        $this->withdrawals = $withdrawals;
        $this->resource = $resource;
    }

    /**
     * For setting the pagination in tables
     *
     * @return Template
     * @throws LocalizedException
     */
    public function _prepareLayout()
    {
        if ($this->getTransactionOrders()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'product.affiliatesales.pager'
            )->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15))->setCollection(
                $this->getTransactionOrders()
            );
            $this->setChild('pager', $pager);
            $this->getTransactionOrders()->load();
        }

        if ($this->getAffiliateProducts()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'product.product.pager'
            )->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15))->setCollection(
                $this->getAffiliateProducts()
            );
            $this->setChild('pager', $pager);
            $this->getAffiliateProducts()->load();
        }
        return parent::_prepareLayout();
    }

    /**
     * Getting the list transactions collection
     *
     * @return AbstractCollection
     */
    public function getTransactionOrders()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;

        $customerEmail = $this->getCustomerSession();
        $collection = $this->_accounts->create();
        $multiselectupdate = $collection->getCollection()
            ->addFieldToFilter('email_address', $customerEmail)->getData();

        $trackingcode = '';

        foreach ($multiselectupdate as $multi) {
            if ($multi['email_address']) {
                $trackingcode = $multi['trackingcode'];
            }
        }
        $orders = $this->transactionorders->create()->getCollection()
            ->addFieldToFilter('trackingcode', $trackingcode);

        $orders->setPageSize($pageSize);
        $orders->setCurPage($page);

        return $orders;
    }

    /**
     * Getting the customer session, checking if who is the logged in user.
     *
     * @return string
     */
    public function getCustomerSession()
    {
        return $this->customerSession->getCustomer()->getEmail();
    }

    /**
     * Getting the list of all affiliate products
     *
     * @return AbstractCollection
     */
    public function getAffiliateProducts()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;

        $collection = $this->_products->getCollection();

        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Getting the affiliates trackingcode
     *
     * @return string
     */
    public function getTrackingcode()
    {
        $customerEmail = $this->getCustomerSession();
        $collection = $this->_accounts->create();
        $multiselectupdate = $collection->getCollection()
            ->addFieldToFilter('email_address', $customerEmail)->getData();

        $trackingcode = '';

        foreach ($multiselectupdate as $multi) {
            if ($multi['email_address']) {
                $trackingcode = $multi['trackingcode'];
            }
        }

        return $trackingcode;
    }

    /**
     * @return mixed
     */
    public function getTrackingUrl()
    {
        return $this->request->getParam('trackingcode');
    }

    /**
     * Save affiliates products
     *
     * @param $trackingcode
     * @param $productsku
     * @param $commission_rate
     */
    public function setAffiliateSku($trackingcode, $productsku, $commission_rate)
    {
        $this->repo->setAffiliateSku($trackingcode, $productsku, $commission_rate);
    }

    /**
     * Get the list of commission rates
     *
     * @return array
     */
    public function getCommisionTitle()
    {
        $collection = $this->commissionrepo->getCommisionTitle();
        return $collection;
    }

    /**
     * Get the total of commissions of every affiliates
     *
     * @return AbstractCollection
     */
    public function getCommissionSum()
    {
        $customerEmail = $this->getCustomerSession();
        $collection = $this->_accounts->create();
        $multiselectupdate = $collection->getCollection()
            ->addFieldToFilter('email_address', $customerEmail)->getData();

        $trackingcode = '';

        foreach ($multiselectupdate as $multi) {
            if ($multi['email_address']) {
                $trackingcode = $multi['trackingcode'];
            }
        }
        $commission = $this->_accounts->create()->getCollection()
            ->addFieldToFilter('trackingcode', $trackingcode);
        return $commission;
    }

    /**
     * Get the reward points of the affiliate
     *
     * @param $trackingcode
     * @return string
     */
    public function getRewards($trackingcode)
    {
        $connection = $this->resource->getConnection();
        $tableName = $connection->getTableName('affiliate_accounts'); //gives table name with prefix

        $reward = "SELECT reward_points FROM $tableName WHERE trackingcode = '$trackingcode'";
        $results = $connection->fetchAll($reward);
        $getreward = 0;
        foreach ($results as $result) {
            $getreward = implode($result);
        }
        return $getreward;
    }

    /**
     *
     */
    public function getWithdrawals()
    {
        $customerEmail = $this->getCustomerSession();
        $collection = $this->_accounts->create();
        $multiselectupdate = $collection->getCollection()
            ->addFieldToFilter('email_address', $customerEmail)->getData();

        $trackingcode = '';

        foreach ($multiselectupdate as $multi) {
            if ($multi['email_address']) {
                $trackingcode = $multi['trackingcode'];
            }
        }
        $withdraw = $this->withdrawals->create()->getCollection()
            ->addFieldToFilter('trackingcode', $trackingcode);
        return $withdraw;
    }

    /**
     * checkingemail
     */
    public function checkEmail()
    {
        $email = $this->customerSession->getCustomer()->getEmail();
        return $this->commissionrepo->checkEmail($email);
    }

}
