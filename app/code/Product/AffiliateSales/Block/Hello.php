<?php
namespace Product\AffiliateSales\Block;

use Magento\Framework\View\Element\Template;
use Product\AffiliateSales\Model\ResourceModel\Accounts\Collection;
use Product\AffiliateSales\Model\ResourceModel\Accounts\CollectionFactory;

class Hello extends Template
{

  private $collectionFactory;


  public function __construct (
      Template\Context $context,
      CollectionFactory $collectionFactory,
      array $data = []
    ) {
      $this->collectionFactory = $collectionFactory;
      parent::__construct($context, $data);
    }

    /**
     * @return \Product\AffiliateSales\Model\Accounts[]
     */
    public function getItems()
    {
      return $this->collectionFactory->create()->getItems();
    }
}
